# Trello-Like Task Board
A Trello-Like Task Board API for santander dio challenge

## Diagrama de classes

```mermaid
classDiagram
    class Board {
        +String name
        +List~List~ lists
    }
    class List {
        +String name
        +List~Card~ cards
    }
    class Card {
        +String title
        +String description
        +User assignedUser
        +List~Comment~ comments
    }
    class User {
        +String name
        +String email
    }
    class Comment {
        +String text
        +User author
    }

    Board "1" --> "*" List : contains
    List "1" --> "*" Card : contains
    Card "1" --> "1" User : assigned to
    Card "1" --> "*" Comment : has
    Comment "1" --> "1" User : written by
```
