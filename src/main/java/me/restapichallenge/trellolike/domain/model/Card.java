package me.restapichallenge.trellolike.domain.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity(name = "tb_card")
@Getter
@Setter
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String title;
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User assignedUser;

    @OneToMany(mappedBy = "card")
    private List<Comment> comments;

    @ManyToOne
    @JoinColumn(name = "listing_id")
    private Listing listing;
}
