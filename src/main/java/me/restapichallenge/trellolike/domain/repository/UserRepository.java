package me.restapichallenge.trellolike.domain.repository;

import me.restapichallenge.trellolike.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);
}
