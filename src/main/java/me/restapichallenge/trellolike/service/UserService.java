package me.restapichallenge.trellolike.service;


import me.restapichallenge.trellolike.domain.model.User;

public interface UserService {
    User findById(Long id);

    User create(User userToCreate);
}
