package me.restapichallenge.trellolike.service.impl;

import lombok.AllArgsConstructor;
import me.restapichallenge.trellolike.domain.model.User;
import me.restapichallenge.trellolike.domain.repository.UserRepository;
import me.restapichallenge.trellolike.service.UserService;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public User create(User userToCreate) {
        if (userRepository.existsByEmail(userToCreate.getEmail()) && userToCreate.getId() != null) {
            throw new IllegalArgumentException("This user already exists!");
        }

        return userRepository.save(userToCreate);
    }
}
